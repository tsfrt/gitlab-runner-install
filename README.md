### Prerequisites

install helm (not needed air-gapped, but not a bad idea to have)

https://helm.sh/docs/intro/install/

install carvel tools (we will want these copied to air-gapped environment too)

https://carvel.dev/#whole-suite

there are a couple sample files in this repo that it will be helpful to have access to

---

### GitLab Runner Prep

### online prep 
add gitlab repo
`helm repo add gitlab https://charts.gitlab.io`

generate a values file
`helm show values gitlab/gitlab-runner > values.yaml`

Review the values file config.  We can refine in the air-gapped environment.

you need to setup your url and token so the gitlab install can interact with the runner.  

also have the CA cert GitLab is using ready

```
# gitlabUrl: http://gitlab.your-domain.com/

## The Registration Token for adding new Runners to the GitLab Server. This must
## be retrieved from your GitLab Instance.
## ref: https://docs.gitlab.com/ce/ci/runners/README.html
##
# runnerRegistrationToken: ""

## The Runner Token for adding new Runners to the GitLab Server. This must
## be retrieved from your GitLab Instance. It is token of already registered runner.
## ref: (we don't yet have docs for that, but we want to use existing token)
##
# runnerToken: ""
```

once happy with your values template your helm chart, this will create a deployment based on your values
`helm template gitlab-runner gitlab/gitlab-runner -n gitlab-runner -f values.yaml > gitlab-runner-install.yaml`

create a lock file with needed images
`kbld -f gitlab-runner-install.yaml --lock-output manifest.lock`

create a bundle of images
`kbld pkg -f manifest.lock --output packaged-images.tar`

move packaged-images.tar, manifest.lock, and gitlab-runner-install.yaml to air-gapped 

### Air-gapped Side

load gitlab images in local repo
`kbld unpkg -f manifest.lock --input packaged-images.tar --repository <your registry>/<optional project>/<gitlab-runner> --lock-output manifest.lock.copied`

create a namespace for your deployment
`kubectl create ns gitlab-runner`

rename image references and apply
`kbld -f manifest.lock.copied -f gitlab-runner-install.yaml | kubectl apply -f- -n gitlab-runner`

---------------

### kapp controller prep

### online prep

this should look familiar to what we did with GitLab except that Kapp-Controller doesn't use Helm.

`curl -L -o kapp-controller.yaml https://github.com/vmware-tanzu/carvel-kapp-controller/releases/latest/download/release.yml`

 
`kbld -f  kapp-controller.yaml --lock-output manifest.lock`


`kbld pkg -f manifest.lock --output packaged-images.tar`

move kapp-controller.yaml, manifest.lock, packaged-images.tar over to air-gapped environment

### Air-gapped Side

load kapp controller images in local repo
`kbld unpkg -f manifest.lock --input packaged-images.tar --repository <your registry>/<optional project>/<kapp-controller> --lock-output manifest.lock.copied`

rename image references and apply
`kbld -f manifest.lock.copied -f kapp-controller.yaml | kubectl apply -f- `
